module "service_accounts" {
  source                = "terraform-google-modules/service-accounts/google"
  project_id            = var.project_id
  names                 = ["cloudmarshal-service-account"]
  project_roles         = [
    "${var.project_id}=>roles/bigquery.dataViewer",
    "${var.project_id}=>roles/viewer"
  ]
  display_name          = "CloudMarshal Service Account"
  description           = "Service integration with CloudMarshal Analyse v1.1"
}

module "bigquery" {
  source  = "terraform-google-modules/bigquery/google"
  dataset_id            = "cm_billing_detailed_export"
  project_id            = var.project_id
  location              = var.bigquery_dataset_location
}
