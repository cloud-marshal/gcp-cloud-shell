variable "project_id" {
  description = "The ID of the project in which to provision resources."
  type        = string
}

variable bigquery_dataset_location {
  description = "The location of the dataset in which billing export will be stored"
  default = "EU"
  type = string
  validation {
    condition = contains(["EU", "US"], var.bigquery_dataset_location)
    error_message = "The bigquery_dataset_location must be one of 'EU' or 'US'."
  }
}