# CloudMarshal GCP Service Configuration

## Before you begin

If you have not been directed to this walkthrough from the CloudMarshal GCP Service Configuration main guide, which is accessible from Administration within the CloudMarshal app, please refer there first before proceeding.

During this walkthrough, you may be prompted to authorise Cloud Shell to make API calls to complete the required actions. Please click the **Authorize** button (**1**) if you see this pop-up.

![Authorise](https://bitbucket.org/cloud-marshal/gcp-cloud-shell/raw/main/init/assets/authorize.png "Authorise")

Click the **Start** button below to begin.

## Project selection

Select the project you'd like to monitor from the dropdown.

<walkthrough-project-setup billing=true></walkthrough-project-setup>

Click the **Next** button below to move to the next step.

## Initialise command

You have chosen **<walkthrough-project-id/>** to monitor with CloudMarshal.

Click the Cloud Shell icon below (<walkthrough-cloud-shell-icon></walkthrough-cloud-shell-icon>) to copy the command to your command line shell and then run it by pressing Enter/Return.

```bash
terraform init
```

You should see _Terraform has been successfully initialized!_ (**1**) in the output of the command.

![Initialized](https://bitbucket.org/cloud-marshal/gcp-cloud-shell/raw/main/init/assets/init.png "Initialized")

Click the **Next** button below to move to the next step.

## Deploy resources

Click the Cloud Shell icon below (<walkthrough-cloud-shell-icon></walkthrough-cloud-shell-icon>) to copy the command to your command line shell and then run it by pressing Enter/Return.

```bash
terraform apply -var="project_id=<walkthrough-project-id/>"
```

The Cloud Shell will show you what it plans to do, and prompt you to accept. Type "**yes**" to accept the plan (**1**) and then run it by pressing Enter/Return.

![Confirm](https://bitbucket.org/cloud-marshal/gcp-cloud-shell/raw/main/init/assets/confirm.png "Confirm")

Click the **Next** button below to move to the next step.

## Deploy complete

After a few seconds, you should see confirmation that the resources have been created (**1**).

![Complete](https://bitbucket.org/cloud-marshal/gcp-cloud-shell/raw/main/init/assets/complete.png "Complete")

The resource deployment process is now complete. Click the link below to be redirected to the Service Accounts page of **{{project-id}}** and refer back to our main guide to complete the GCP service configuration process.

[Go to Service Accounts page](https://console.cloud.google.com/iam-admin/serviceaccounts?project={{project-id}})
